require_relative '../CsScript.rb'

describe "CsScript" do
    it "#proccess_should_return_pow" do
        testClass = CsScript.new()
        result = testClass.Proccess("CCS")
        expect(result).to eq(8)
    end
    it "#proccess_should_return_reverse_word" do
        testClass = CsScript.new()
        result = testClass.Proccess("Test")
        expect(result).to match("tseT")
    end
end