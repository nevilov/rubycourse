require_relative '../Greeting.rb'

describe "Greeting" do
    it "#greet_when_age_is_greater_18" do
        testClass = Greeting.new()
        result = testClass.greet("Test", 18)
        expect(result).to eq("Hello Test. Good time to work.")
    end
    it "#gree_when_age_is_under_18" do
        testClass = Greeting.new()
        result = testClass.greet("Test", 15);
        expect(result).to eq("Hello Test. You are under 18 but It's never too early to learn to program.")
    end
end