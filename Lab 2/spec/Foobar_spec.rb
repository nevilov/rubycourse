require_relative '../Foobar.rb'

describe "Foobar" do
    it "calculate_when_array_contain_20" do
        testClass = Foobar.new()
        arrayOfDigits = [1, 5, 10, 15, 20]
        result = testClass.calculate(arrayOfDigits)
        expect(result).to eq(5)
    end
    it "calculate_when_array_not_contain_20" do
        testClass = Foobar.new()
        arrayOfDigits = [1, 5, 10, 15]
        result = testClass.calculate(arrayOfDigits)
        expect(result).to eq(31)
    end
end