class Greeting
    def greet(username, age)
        if age < 18
            resultText = "Hello #{username}. You are under 18 but It's never too early to learn to program."
            puts resultText
            return resultText
        else
            resultText = "Hello #{username}. Good time to work."
            puts resultText
            return resultText
        end
    end
end
